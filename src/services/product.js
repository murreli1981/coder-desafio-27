const productModel = require("../dao/models/product");

module.exports = class {
  async addProduct(product) {
    const productCreated = await productModel.create(product);
    console.log(productCreated);
    return productCreated;
  }

  async getProduct(id) {
    return productModel.findById(id);
  }

  async getAllProducts() {
    return productModel.find();
  }

  async updateProduct(id, payload) {
    const productEdited = await productModel.findByIdAndUpdate(id, payload);
    console.log(productEdited);
    return productEdited;
  }

  async deleteProduct(id) {
    const productDeleted = await productModel.findByIdAndDelete(id);
    console.log(productDeleted);
    return productDeleted;
  }
};

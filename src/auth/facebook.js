const userModel = require("../dao/models/user");
const { FB_CLIENT_ID, FB_CLIENT_SECRET } = require("../config/globals");

const passport = require("passport");
const FacebookStrategy = require("passport-facebook").Strategy;

const strategy = new FacebookStrategy(
  {
    clientID: FB_CLIENT_ID,
    clientSecret: FB_CLIENT_SECRET,
    callbackURL: "http://localhost:9000/auth/facebook/callback",
    profileFields: ["id", "displayName", "photos", "email"],
  },
  function (accessToken, refreshToken, profile, done) {
    const user = {
      facebookId: profile.id,
      username: profile.displayName,
    };
    userModel.findOrCreate(user, (err, user) => {
      if (err) {
        return done(err);
      }
      user.email = profile.emails[0].value;
      user.fb = profile.photos[0].value;
      return done(null, user);
    });
  }
);

passport.use(strategy);

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});
